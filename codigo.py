 
import bpy
from math import pi, radians, degrees
import os

#######################
## VARIAVEIS GLOBAIS ##
#######################
tamanho_multiplier_universal = 1.0  # Muda o tamanho de todos os objetos em existencia, default = 1.0
tamanho_multiplier_the_old_one = 0.15 * tamanho_multiplier_universal  # Muda a escala do ACTOR, default = 1.0
tamanho_multiplier_the_nameless_one = 0.18 * tamanho_multiplier_universal  # Muda a escala do ACTOR, default = 1.0
tamanho_multiplier = 1.0 * tamanho_multiplier_universal
tamanho_multiplier_poste_luz = 1.0 * tamanho_multiplier_universal

########### USEFULL CONSTANTS #################

POSING_COORDINATES = (-1.0, 1.0, 0.5)  # Origin XYZ for posing
TIG_NAME = "Armature_TIG"  # Name of TIG armature
#  [child, parent, size_x, size_y, size_z, gap_x, gap_y, gap_z]
TIG_PARTS_LIST = [['.TIG_Cabeca', 'root', -0.1, 0, 0, 0, 0, -0.25],
                  ['.TIG_Torso', '.TIG_Cabeca', 0, 0, -0.1, 0, 0, -0.25],

                  ['.TIG_Braco_Direito_Superior', '.TIG_Torso', 0, 0, -0.6, 0, -0.525, -0.1],
                  ['.TIG_Braco_Direito_Inferior', '.TIG_Braco_Direito_Superior', 0, 0, -0.35, 0, 0, -0.1],
                  ['.TIG_Braco_Direito_Mao', '.TIG_Braco_Direito_Inferior', 0, 0, -0.075, 0, 0.01, -0.035],

                  ['.TIG_Braco_Esquerdo_Superior', '.TIG_Torso', 0, 0, -0.6, 0, 0.525, -0.1],
                  ['.TIG_Braco_Esquerdo_Inferior', '.TIG_Braco_Esquerdo_Superior', 0, 0, -0.35, 0, 0, -0.1],
                  ['.TIG_Braco_Esquerdo_Mao', '.TIG_Braco_Esquerdo_Inferior', 0, 0, -0.075, 0, 0.01, -0.035],

                  ['.TIG_Perna_Direita', '.TIG_Torso', 0, 0, -1, 0, -0.35, -1.15],

                  ['.TIG_Perna_Esquerda', '.TIG_Torso', 0, 0, -1, 0, 0.35, -1.15],
                  ]  # List of parts used for posing

# Create the Armature what is essentially a structured colection of bones
# For putting the bones inside we need go in the Edit-Mode
def makeBaseArmature(designation='Skeleton'):
    bpy.ops.object.add(type='ARMATURE', enter_editmode=True, location=(3.0, 3.20, 2.75))
    ob = bpy.context.object
    ob.name = designation
    return ob


# Joining the bones as defined in the list with the name 'extremity_bones' into the 
# Armature given by armatureHandle
def joiningBones(armatureHandle, bonesData):
    
    if (bonesData[0][1] == 'root') == False:
        print('The first bone has to be a root!')
        return (0)
    for i in range(len(bonesData)):
        # show the names of all bones in 3D-Editor
        '''armatureHandle.show_names = True'''
        bone = armatureHandle.edit_bones.new(bonesData[i][0])
        if bonesData[i][1] == 'root':
            bone.head = (0,0,0)
            bone.tail = (bonesData[i][2], bonesData[i][3], bonesData[i][2])
        else:
            bone.parent = armatureHandle.edit_bones[bonesData[i][1]]
            # The position of the child's head is that of the parent tail 
            x = bone.parent.tail[0]
            y = bone.parent.tail[1]
            z = bone.parent.tail[2]
            # Regarding the case if there is a gap between parent bone and child bone
            if len(bonesData[i]) > 5: x += bonesData[i][5]
            if len(bonesData[i]) > 6: y += bonesData[i][6]
            if len(bonesData[i]) > 5: z += bonesData[i][7]
            bone.head = (x, y, z)
            # The position of the child´s tail is the position of its head plus the extension given in the 'bonesData'
            bone.tail = (bone.head[0] + bonesData[i][2], bone.head[1] + bonesData[i][3], bone.head[2] + bonesData[i][4])

def walk_right(bone_name, key='N', action_name="action_walk_right"):
    armature = "Armature_TIG"
    # Set Pose-mode
    bpy.ops.object.mode_set(mode='POSE')
    
    sensors = bpy.data.objects[armature].game.sensors
    controllers = bpy.data.objects[armature].game.controllers
    actuators = bpy.data.objects[armature].game.actuators
    
    #Sensors
    bpy.ops.logic.sensor_add(type="KEYBOARD", object=armature)
    #Controllers
    bpy.ops.logic.controller_add(type="LOGIC_OR", object=armature)
    #Actuators
    bpy.ops.logic.actuator_add(type="ACTION", object=armature)
    
    # Newly added logic blocks will be the last ones:
    sensor = sensors[-1]
    controller = controllers[-1]
    actuator = actuators[-1]

    sensor.link(controller)
    actuator.link(controller)

    size = len(bpy.data.objects[armature].game.sensors)
    size -= 1
    sensors[size].key = key
    location = bpy.data.objects[armature].pose.bones[bone_name[2]].location
    total_inc = 0.05
    
    #Whole fuselage
    bpy.data.objects[armature].pose.bones[bone_name[2]].keyframe_insert('location', frame=0, group=bone_name[2])
    
    bpy.data.objects[armature].pose.bones[bone_name[2]].keyframe_insert('location', frame=1, group=bone_name[2])
    
    bpy.data.objects[armature].pose.bones[bone_name[2]].location = (location[0]-total_inc,location[1]+total_inc,location[2]+total_inc)
    bpy.data.objects[armature].pose.bones[bone_name[2]].keyframe_insert('location', frame=10, group=bone_name[2])
    
    bpy.data.objects[armature].pose.bones[bone_name[2]].location = (location[0],location[1],location[2])
    bpy.data.objects[armature].pose.bones[bone_name[2]].keyframe_insert('location', frame=20, group=bone_name[2])    


    #Right Leg
    bpy.data.objects[armature].pose.bones[bone_name[1]].keyframe_insert('rotation_euler', frame=0, group=bone_name[1])
    
    bpy.data.objects[armature].pose.bones[bone_name[1]].keyframe_insert('rotation_euler', frame=1, group=bone_name[1])
    
    bpy.data.objects[armature].pose.bones[bone_name[1]].rotation_euler = (0,0,radians(45))
    bpy.data.objects[armature].pose.bones[bone_name[1]].keyframe_insert('rotation_euler', frame=10, group=bone_name[1])
    
    bpy.data.objects[armature].pose.bones[bone_name[1]].rotation_euler = (0,0,radians(-30))
    bpy.data.objects[armature].pose.bones[bone_name[1]].keyframe_insert('rotation_euler', frame=15, group=bone_name[1])    
    
    bpy.data.objects[armature].pose.bones[bone_name[1]].rotation_euler = (0,0,0)
    bpy.data.objects[armature].pose.bones[bone_name[1]].keyframe_insert('rotation_euler', frame=20, group=bone_name[1])    


    #Left Leg
    bpy.data.objects[armature].pose.bones[bone_name[0]].keyframe_insert('rotation_euler', frame=0, group=bone_name[0])
    
    bpy.data.objects[armature].pose.bones[bone_name[0]].keyframe_insert('rotation_euler', frame=1, group=bone_name[0])

    bpy.data.objects[armature].pose.bones[bone_name[0]].rotation_euler = (0,0,radians(45))
    bpy.data.objects[armature].pose.bones[bone_name[0]].keyframe_insert('rotation_euler', frame=10, group=bone_name[0])
    
    bpy.data.objects[armature].pose.bones[bone_name[0]].rotation_euler = (0,0,radians(-30))
    bpy.data.objects[armature].pose.bones[bone_name[0]].keyframe_insert('rotation_euler', frame=15, group=bone_name[0])    
    
    bpy.data.objects[armature].pose.bones[bone_name[0]].rotation_euler = (0,0,0)
    bpy.data.objects[armature].pose.bones[bone_name[0]].keyframe_insert('rotation_euler', frame=20, group=bone_name[0])    

    
    bpy.data.actions[size].name = action_name
    actuator.action = bpy.data.actions[action_name]
    actuator.frame_end = 20

def walk_left(bone_name, key='B', action_name="action_walk_left"):
    armature = "Armature_TIG"
    # Set Pose-mode
    bpy.ops.object.mode_set(mode='POSE')
    
    sensors = bpy.data.objects[armature].game.sensors
    controllers = bpy.data.objects[armature].game.controllers
    actuators = bpy.data.objects[armature].game.actuators
    
    #Sensors
    bpy.ops.logic.sensor_add(type="KEYBOARD", object=armature)
    #Controllers
    bpy.ops.logic.controller_add(type="LOGIC_OR", object=armature)
    #Actuators
    bpy.ops.logic.actuator_add(type="ACTION", object=armature)
    
    # Newly added logic blocks will be the last ones:
    sensor = sensors[-1]
    controller = controllers[-1]
    actuator = actuators[-1]

    sensor.link(controller)
    actuator.link(controller)

    size = len(bpy.data.objects[armature].game.sensors)
    size -= 1
    sensors[size].key = key
    location = bpy.data.objects[armature].pose.bones[bone_name[2]].location
    #location = (-0.3,0.085,-0.2)
    total_inc = 0.05
    
    #Whole fuselage
    bpy.data.objects[armature].pose.bones[bone_name[2]].keyframe_insert('location', frame=0, group=bone_name[2])
    
    bpy.data.objects[armature].pose.bones[bone_name[2]].keyframe_insert('location', frame=1, group=bone_name[2])
    
    bpy.data.objects[armature].pose.bones[bone_name[2]].location = (location[0]-total_inc,location[1]+total_inc,location[2]+total_inc)
    bpy.data.objects[armature].pose.bones[bone_name[2]].keyframe_insert('location', frame=10, group=bone_name[2])
    
    bpy.data.objects[armature].pose.bones[bone_name[2]].location = (location[0],location[1],location[2])
    bpy.data.objects[armature].pose.bones[bone_name[2]].keyframe_insert('location', frame=20, group=bone_name[2])    


    #Right Leg
    bpy.data.objects[armature].pose.bones[bone_name[1]].keyframe_insert('rotation_euler', frame=0, group=bone_name[1])
    
    bpy.data.objects[armature].pose.bones[bone_name[1]].keyframe_insert('rotation_euler', frame=1, group=bone_name[1])

    
    bpy.data.objects[armature].pose.bones[bone_name[1]].rotation_euler = (0,0,radians(45))
    bpy.data.objects[armature].pose.bones[bone_name[1]].keyframe_insert('rotation_euler', frame=10, group=bone_name[1])
    
    bpy.data.objects[armature].pose.bones[bone_name[1]].rotation_euler = (0,0,radians(-30))
    bpy.data.objects[armature].pose.bones[bone_name[1]].keyframe_insert('rotation_euler', frame=15, group=bone_name[1])    
    
    bpy.data.objects[armature].pose.bones[bone_name[1]].rotation_euler = (0,0,radians(0))
    bpy.data.objects[armature].pose.bones[bone_name[1]].keyframe_insert('rotation_euler', frame=20, group=bone_name[1])    


    #Left Leg
    bpy.data.objects[armature].pose.bones[bone_name[0]].keyframe_insert('rotation_euler', frame=0, group=bone_name[0])
    
    bpy.data.objects[armature].pose.bones[bone_name[0]].keyframe_insert('rotation_euler', frame=1, group=bone_name[0])

    
    bpy.data.objects[armature].pose.bones[bone_name[0]].rotation_euler = (0,0,radians(45))
    bpy.data.objects[armature].pose.bones[bone_name[0]].keyframe_insert('rotation_euler', frame=10, group=bone_name[0])
    
    bpy.data.objects[armature].pose.bones[bone_name[0]].rotation_euler = (0,0,radians(-30))
    bpy.data.objects[armature].pose.bones[bone_name[0]].keyframe_insert('rotation_euler', frame=15, group=bone_name[0])    
    
    bpy.data.objects[armature].pose.bones[bone_name[0]].rotation_euler = (0,0,0)
    bpy.data.objects[armature].pose.bones[bone_name[0]].keyframe_insert('rotation_euler', frame=20, group=bone_name[0])    

    
    bpy.data.actions[size].name = action_name
    actuator.action = bpy.data.actions[action_name]
    actuator.frame_end = 20
    
#TIG shot
def shot(bone_name=".TIG_Braco_Direito_Inferior", key='P', action_name="action_shot_right"):
    armature="Armature_TIG"
    # Set Pose-mode
    bpy.ops.object.mode_set(mode='POSE')

    sensors = bpy.data.objects[armature].game.sensors
    controllers = bpy.data.objects[armature].game.controllers
    actuators = bpy.data.objects[armature].game.actuators
    #Sensors
    bpy.ops.logic.sensor_add(type="KEYBOARD", object=armature)
    #Controllers
    bpy.ops.logic.controller_add(type="LOGIC_OR", object=armature)
    #Actuators
    actuator = bpy.ops.logic.actuator_add(type="ACTION", object=armature)
    
    # Newly added logic blocks will be the last ones:
    sensor = sensors[-1]
    controller = controllers[-1]
    actuator = actuators[-1]

    sensor.link(controller)
    actuator.link(controller)

    size = len(bpy.data.objects[armature].game.sensors)
    size -= 1
    sensors[size].key = key
    
    location = bpy.data.objects[armature].pose.bones[bone_name].location
    #Shot animation

    bpy.data.objects[armature].pose.bones[bone_name].rotation_euler = (0,0,0)
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('location', frame=0, group=bone_name)
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('rotation_euler', frame=0, group=bone_name)
    
    bpy.data.objects[armature].pose.bones[bone_name].rotation_euler = (0,0,0)
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('location', frame=1, group=bone_name)
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('rotation_euler', frame=1, group=bone_name)
    
    bpy.data.objects[armature].pose.bones[bone_name].rotation_euler = (0,0,radians(90))
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('rotation_euler', frame=4, group=bone_name)
    
    bpy.data.objects[armature].pose.bones[bone_name].location = (location[0]-5,0,0)
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('location', frame=8, group=bone_name)

    bpy.data.objects[armature].pose.bones[bone_name].location = (location[0]-5,0,0)
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('location', frame=12, group=bone_name)    
    
    bpy.data.objects[armature].pose.bones[bone_name].location = (location[0]+10.0,0,0)
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('location', frame=16, group=bone_name)    

    bpy.data.objects[armature].pose.bones[bone_name].rotation_euler = (0,0,0)
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('rotation_euler', frame=20, group=bone_name)
    
    
    bpy.data.actions[size].name = action_name
    actuator.action = bpy.data.actions[action_name]
    actuator.frame_end = 20

#TIG kick
def kick(bone_name=".TIG_Perna_Direita", key='L', action_name="action_kick_right"):
    armature="Armature_TIG"
    sensors = bpy.data.objects[armature].game.sensors
    controllers = bpy.data.objects[armature].game.controllers
    actuators = bpy.data.objects[armature].game.actuators
    #Sensors
    bpy.ops.logic.sensor_add(type="KEYBOARD", object=armature)
    #Controllers
    bpy.ops.logic.controller_add(type="LOGIC_OR", object=armature)
    #Actuators
    actuator = bpy.ops.logic.actuator_add(type="ACTION", object=armature)
    
    # Newly added logic blocks will be the last ones:
    sensor = sensors[-1]
    controller = controllers[-1]
    actuator = actuators[-1]

    sensor.link(controller)
    actuator.link(controller)

    size = len(bpy.data.objects[armature].game.sensors)
    size -= 1
    sensors[size].key = key

    #Animacao Perna
    bpy.data.objects[armature].pose.bones[bone_name].rotation_euler = (0,0,0)
    
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('location', frame=0, group=bone_name)
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('rotation_euler', frame=0, group=bone_name)

    bpy.data.objects[armature].pose.bones[bone_name].rotation_euler = (0,0,0)
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('location', frame=1, group=bone_name)
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('rotation_euler', frame=1, group=bone_name)
    
    bpy.data.objects[armature].pose.bones[bone_name].rotation_euler = (0,0,radians(60))
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('rotation_euler', frame=10, group=bone_name)
    
    bpy.data.objects[armature].pose.bones[bone_name].rotation_euler = (0,0,0)
    bpy.data.objects[armature].pose.bones[bone_name].keyframe_insert('rotation_euler', frame=20, group=bone_name)    
    
    bpy.data.actions[size].name = action_name
    actuator.action = bpy.data.actions[action_name]
    actuator.frame_end = 20


def setLocationRotationTIG():
    """
    Sets a new location to all the pieces of TIG.
    @param location : List --> List of all the locations to update TIG with.
    """
    bpy.data.objects[".TIG_Cabeca"].location = (0.07, 0.07 ,-0.07)
    bpy.data.objects[".TIG_Cabeca"].rotation_euler = (radians(55), radians(30), radians(-55))
    
    bpy.data.objects[".TIG_Torso"].location = (0.025, 1.1 , 0)
    bpy.data.objects[".TIG_Torso"].rotation_euler = (radians(180), radians(0), radians(0))

    bpy.data.objects[".TIG_Braco_Direito_Superior"].location = (0.01, -0.55 , 0.05)
    bpy.data.objects[".TIG_Braco_Direito_Superior"].rotation_euler = (radians(180), radians(0), radians(0))

    bpy.data.objects[".TIG_Braco_Direito_Inferior"].location = (0.015, -0.2 ,-0.01)
    bpy.data.objects[".TIG_Braco_Direito_Inferior"].rotation_euler = (radians(90), radians(45), radians(0))

    bpy.data.objects[".TIG_Braco_Direito_Mao"].location = (-0.04, -0.06 ,-0.01)
    bpy.data.objects[".TIG_Braco_Direito_Mao"].rotation_euler = (radians(90), radians(0), radians(90))

    bpy.data.objects[".TIG_Braco_Esquerdo_Superior"].location = (0.01, -0.55 , -0.05)
    bpy.data.objects[".TIG_Braco_Esquerdo_Superior"].rotation_euler = (radians(180), radians(0), radians(0))

    bpy.data.objects[".TIG_Braco_Esquerdo_Inferior"].location = (0.015, -0.2 ,0.01)
    bpy.data.objects[".TIG_Braco_Esquerdo_Inferior"].rotation_euler = (radians(90), radians(45), radians(0))

    bpy.data.objects[".TIG_Braco_Esquerdo_Mao"].location = (-0.04, -0.06 ,0.01)
    bpy.data.objects[".TIG_Braco_Esquerdo_Mao"].rotation_euler = (radians(90), radians(0), radians(90))

    bpy.data.objects[".TIG_Perna_Direita"].location = (0.03, -1 ,-0.015)
    bpy.data.objects[".TIG_Perna_Direita"].rotation_euler = (radians(180), radians(0), radians(0))

    bpy.data.objects[".TIG_Perna_Esquerda"].location = (0.03, -1 ,0.015)
    bpy.data.objects[".TIG_Perna_Esquerda"].rotation_euler = (radians(180), radians(0), radians(0))

def anexObjects(selector=0):
    '''
    Looks for the objects already created in the scene, adds them to a list, and returns the list.
    '''
    objects_list = []
    for i in bpy.context.scene.objects:
        if selector == 0:  # TIG
            if ".TIG" in i.name:
                if "Olho" not in i.name and "Boca" not in i.name:
                    objects_list.append(i)
        elif selector == 1:  # TNO
            if "TNO" in bpy.context.scene:
                objects_list.append(i)
        else:
            print("Invalid parameter for selector passed: ", selector, " was provided!")
    return objects_list


def parenter(list_of_bones):
    '''
    Takes all the hulls and bones and parents them to each other
    '''
    for i in list_of_bones:
        bpy.data.objects[i[0]].parent = bpy.data.objects["Armature_TIG"]
        bpy.ops.object.mode_set(mode='POSE')
        bpy.data.objects[i[0]].parent_type = 'BONE'
        bpy.data.objects[i[0]].parent_bone = i[0]
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')


def main(name_armature=TIG_NAME, list_of_bones=TIG_PARTS_LIST):
    hulls = anexObjects()
    # ad 1) The armature is now avaiable under 'obj'
    obj = makeBaseArmature(designation=name_armature)
    # And the data avaiable under 'reference'
    reference = obj.data

    # ad 2) Put the bones as defined in the list inside the Armatures
    #       under 'reference'
    joiningBones(reference, list_of_bones)

    # ad 3) Joining the bones with the hull objects
    parenter(list_of_bones)
    #Corrects a location bug
    setLocationRotationTIG()
    #Adds a right shot
    shot()
    shot('.TIG_Braco_Esquerdo_Inferior', 'O', "action_shot_left")

    kick()   
    kick('.TIG_Perna_Esquerda', 'K', "action_kick_left")
    
    walk_left(['.TIG_Perna_Esquerda','.TIG_Perna_Direita','.TIG_Cabeca'])
    walk_right(['.TIG_Perna_Esquerda','.TIG_Perna_Direita','.TIG_Cabeca'])

########################
## FUNÇÕES PRIMITIVAS ##
########################

# Junta os objetos tornando-os num so
def join_objects(objects):
    # Deselect all object
    bpy.ops.object.select_all(action='DESELECT')
    # Select all the objects of the list "objects"
    for i in range(0, len(objects)):
        # print(i)
        bpy.data.objects[objects[i].name].select = True
    # Join all objects
    bpy.ops.object.join()
    # Catch the juncton object under the name "obj" and pass it
    obj = bpy.context.scene.objects.active
    return obj


# Junta os objetos atraves de outro metodo, o codigo feito da 1 funcao por vezes nao funcionava, adaptado da net
# Source: https://blender.stackexchange.com/questions/13986/how-to-join-objects-with-python
def join_objects_v2(objects):
    # Deselect all object
    bpy.ops.object.select_all(action='DESELECT')
    # Select all the objects of the list "objects"
    for i in objects:
        # print(i)
        if i.type == 'MESH':
            i.select = True
            bpy.context.scene.objects.active = i
        else:
            i.select = False
    # Join all objects
    bpy.ops.object.join()
    return i


def delete_all_meshes_except_TIG():
    # select objects by type
    for o in bpy.data.objects:
        if (o.type == 'MESH' and "TNO" in o.name):
            o.select = True
        else:
            o.select = False
    # call the operator once
    bpy.ops.object.delete()

def delete_all_empty():
    # select objects by type
    for o in bpy.data.objects:
        if o.type == 'EMPTY':
            o.select = True
        else:
            o.select = False
    # call the operator once
    bpy.ops.object.delete()

def delete_object_list(object_list):
    # select objects by type
    for o in object_list:
        o.select = True
    bpy.ops.object.delete()

# Criacao de um objeto vazio
def add_empty(loc):
    bpy.ops.object.empty_add(location=loc)
    return bpy.context.active_object


# Cricao de um objeto tipo cubo.Dois tuples-SIZE(size_x,size_y,size_z) || LOCATION(x,y,z)
def cube(size=(1, 1, 1), location=(0, 0, 0)):
    # Create one long Cube
    bpy.ops.mesh.primitive_cube_add(radius=1)
    c = bpy.context.scene.objects.active
    c.scale = (size[0], size[1], size[2])
    c.location = (location[0], location[1], location[2] + c.scale.z)
    return c


# Criacao de um objeto tipo cilindro
def cylinder(size=(1, 1, 1), location=(0, 0, 0)):
    bpy.ops.mesh.primitive_cylinder_add(radius=1)
    cy = bpy.context.scene.objects.active
    cy.scale = (size[0], size[1], size[2])
    cy.location = (location[0], location[1], location[2] + cy.scale.z)
    return cy


def sphere(s_size=1.0, location=(0, 0, 0)):
    bpy.ops.mesh.primitive_uv_sphere_add(size=s_size)
    uvs = bpy.context.scene.objects.active
    uvs.location = (location[0], location[1], location[2])
    return uvs


def difference(first, second):
    modifier = first.modifiers.new('Modifier', 'BOOLEAN')
    modifier.object = second
    modifier.operation = 'DIFFERENCE'
    bpy.ops.object.modifier_apply(apply_as='DATA', modifier=modifier.name)
    scene = bpy.context.scene
    scene.objects.unlink(second)
    # Linha adicionada para conseguir fazer varios cortes sucessivos
    return first


def the_nameless_one(tamanho, coordenadas_xyz):
    # Cabeça
    c1 = cylinder(
        [(tamanho[0] - 0) * tamanho_multiplier_the_nameless_one, (tamanho[1] - 0) * tamanho_multiplier_the_nameless_one,
         (tamanho[2] + 1) * tamanho_multiplier_the_nameless_one],
        [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
         (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
         (coordenadas_xyz[2] - 1.2) * tamanho_multiplier_the_nameless_one])
    c1.rotation_euler = (pi / 2, 0, 0)
    # Antena
    antena1 = cylinder([(tamanho[0] - 0.95) * tamanho_multiplier_the_nameless_one,
                        (tamanho[1] - 0.95) * tamanho_multiplier_the_nameless_one,
                        (tamanho[2] - 0.5) * tamanho_multiplier_the_nameless_one],
                       [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                        (coordenadas_xyz[1] + 0.2) * tamanho_multiplier_the_nameless_one,
                        (coordenadas_xyz[2] + 1.7) * tamanho_multiplier_the_nameless_one])
    antena1.rotation_euler = (pi / 1.1, 0, 0)
    antena2 = cylinder([(tamanho[0] - 0.95) * tamanho_multiplier_the_nameless_one,
                        (tamanho[1] - 0.95) * tamanho_multiplier_the_nameless_one,
                        (tamanho[2] - 0.5) * tamanho_multiplier_the_nameless_one],
                       [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                        (coordenadas_xyz[1] - 0.2) * tamanho_multiplier_the_nameless_one,
                        (coordenadas_xyz[2] + 1.7) * tamanho_multiplier_the_nameless_one])
    antena2.rotation_euler = (pi / -1.1, 0, 0)
    pompom1 = sphere((tamanho[0] - 0.8) * tamanho_multiplier_the_nameless_one,
                     [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                      (coordenadas_xyz[1] + 0.35) * tamanho_multiplier_the_nameless_one,
                      (coordenadas_xyz[2] + 2.7) * tamanho_multiplier_the_nameless_one])
    pompom2 = sphere((tamanho[0] - 0.8) * tamanho_multiplier_the_nameless_one,
                     [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                      (coordenadas_xyz[1] - 0.35) * tamanho_multiplier_the_nameless_one,
                      (coordenadas_xyz[2] + 2.7) * tamanho_multiplier_the_nameless_one])

    # Olhos
    # Direito
    olh_d = cylinder([(tamanho[0] - 0.6) * tamanho_multiplier_the_nameless_one,
                      (tamanho[1] - 0.6) * tamanho_multiplier_the_nameless_one,
                      (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                     [(coordenadas_xyz[0] + 0.9) * tamanho_multiplier_the_nameless_one,
                      (coordenadas_xyz[1] - 0.8) * tamanho_multiplier_the_nameless_one,
                      (coordenadas_xyz[2] + 0.9) * tamanho_multiplier_the_nameless_one])
    olh_d.rotation_euler = (0, pi / 2, 0)
    # Esquerdo
    olh_e = cylinder([(tamanho[0] - 0.6) * tamanho_multiplier_the_nameless_one,
                      (tamanho[1] - 0.6) * tamanho_multiplier_the_nameless_one,
                      (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                     [(coordenadas_xyz[0] + 0.9) * tamanho_multiplier_the_nameless_one,
                      (coordenadas_xyz[1] + 0.8) * tamanho_multiplier_the_nameless_one,
                      (coordenadas_xyz[2] + 0.9) * tamanho_multiplier_the_nameless_one])
    olh_e.rotation_euler = (0, pi / 2, 0)

    # Boca#
    b = cube([(tamanho[0] - 0.7) * tamanho_multiplier_the_nameless_one,
              (tamanho[1] - 0.4) * tamanho_multiplier_the_nameless_one,
              (tamanho[2] - 0.9) * tamanho_multiplier_the_nameless_one],
             [coordenadas_xyz[0] + 0.8 * tamanho_multiplier_the_nameless_one,
              (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
              (coordenadas_xyz[2] + 0.3) * tamanho_multiplier_the_nameless_one])

    # Orlhas
    ore_d = sphere((tamanho[0] - 0.5) * tamanho_multiplier_the_nameless_one,
                   [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                    (coordenadas_xyz[1] - 2) * tamanho_multiplier_the_nameless_one,
                    (coordenadas_xyz[2] + 0.8) * tamanho_multiplier_the_nameless_one])
    ore_e = sphere((tamanho[0] - 0.5) * tamanho_multiplier_the_nameless_one,
                   [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                    (coordenadas_xyz[1] + 2) * tamanho_multiplier_the_nameless_one,
                    (coordenadas_xyz[2] + 0.8) * tamanho_multiplier_the_nameless_one])

    # Pescoço
    p1 = cylinder([(tamanho[0] - 0.6) * tamanho_multiplier_the_nameless_one,
                   (tamanho[1] - 0.5) * tamanho_multiplier_the_nameless_one,
                   (tamanho[2] - 0.95) * tamanho_multiplier_the_nameless_one],
                  [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                   (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                   (coordenadas_xyz[2] - 0.3) * tamanho_multiplier_the_nameless_one])
    p2 = cylinder([(tamanho[0] - 0.7) * tamanho_multiplier_the_nameless_one,
                   (tamanho[1] - 0.6) * tamanho_multiplier_the_nameless_one,
                   (tamanho[2] - 0.7) * tamanho_multiplier_the_nameless_one],
                  [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                   (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                   (coordenadas_xyz[2] - 0.6) * tamanho_multiplier_the_nameless_one])
    p3 = cylinder([(tamanho[0] - 0.6) * tamanho_multiplier_the_nameless_one,
                   (tamanho[1] - 0.5) * tamanho_multiplier_the_nameless_one,
                   (tamanho[2] - 0.95) * tamanho_multiplier_the_nameless_one],
                  [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                   (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                   (coordenadas_xyz[2] - 0.5) * tamanho_multiplier_the_nameless_one])

    # Torso
    # Torso Superior
    t_s1 = cube([(tamanho[0] - 0.3) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] + 0.4) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 1) * tamanho_multiplier_the_nameless_one])
    t_s2 = cube([(tamanho[0] - 0.1) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] + 0.8) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 1.4) * tamanho_multiplier_the_nameless_one])
    t_s3 = cube([(tamanho[0] + 0.1) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] + 1.2) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 1.8) * tamanho_multiplier_the_nameless_one])

    t_s4 = cube([(tamanho[0] + 0.3) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] + 1.6) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 2.2) * tamanho_multiplier_the_nameless_one])
    t_s5 = cube([(tamanho[0] + 0.1) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] + 1.2) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 2.6) * tamanho_multiplier_the_nameless_one])
    t_s6 = cube([(tamanho[0] - 0.1) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] + 0.8) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 3) * tamanho_multiplier_the_nameless_one])
    t_s7 = cube([(tamanho[0] - 0.3) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] + 0.4) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 3.4) * tamanho_multiplier_the_nameless_one])
    t_s8 = cube([(tamanho[0] - 0.5) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] + 0) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 3.8) * tamanho_multiplier_the_nameless_one])
    t_s9 = cube([(tamanho[0] - 0.7) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] - 0.4) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 4.2) * tamanho_multiplier_the_nameless_one])

    # Torso Central
    t_c = sphere((tamanho[0] - 0.1) * tamanho_multiplier_the_nameless_one,
                 [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[2] - 4.7) * tamanho_multiplier_the_nameless_one])
    # Torso Inferior
    t_i1 = cube([(tamanho[0] - 0.7) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] - 0.4) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 5.4) * tamanho_multiplier_the_nameless_one])
    t_i2 = cube([(tamanho[0] - 0.5) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] + 0) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 5.8) * tamanho_multiplier_the_nameless_one])
    t_i3 = cube([(tamanho[0] - 0.3) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] + 0.4) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 6.2) * tamanho_multiplier_the_nameless_one])
    t_i4 = cube([(tamanho[0] - 0.1) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] + 0.6) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 6.6) * tamanho_multiplier_the_nameless_one])
    t_i5 = cube([(tamanho[0] - 0.3) * tamanho_multiplier_the_nameless_one,
                 (tamanho[1] + 0.4) * tamanho_multiplier_the_nameless_one,
                 (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[1]) * tamanho_multiplier_the_nameless_one,
                 (coordenadas_xyz[2] - 7) * tamanho_multiplier_the_nameless_one])

    # Ombro Direito
    om_d = sphere((tamanho[0]) * tamanho_multiplier_the_nameless_one,
                  [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                   (coordenadas_xyz[1] - 2.5) * tamanho_multiplier_the_nameless_one,
                   (coordenadas_xyz[2] - 1.6) * tamanho_multiplier_the_nameless_one])
    # Ombro Esquerdo
    om_e = sphere((tamanho[0]) * tamanho_multiplier_the_nameless_one,
                  [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                   (coordenadas_xyz[1] + 2.5) * tamanho_multiplier_the_nameless_one,
                   (coordenadas_xyz[2] - 1.6) * tamanho_multiplier_the_nameless_one])

    # Anca Direita
    a_d = sphere((tamanho[0]) * tamanho_multiplier_the_nameless_one,
                 [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[1] - 1.8) * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[2] - 6.3) * tamanho_multiplier_the_nameless_one])
    # Anca Esquerda
    a_e = sphere((tamanho[0]) * tamanho_multiplier_the_nameless_one,
                 [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[1] + 1.8) * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[2] - 6.3) * tamanho_multiplier_the_nameless_one])

    # Cotovelo Direito
    c_d = sphere((tamanho[0] - 0.5) * tamanho_multiplier_the_nameless_one,
                 [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[1] - 5) * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[2] - 1.6) * tamanho_multiplier_the_nameless_one])
    # Cotovelo Esquerdo
    c_e = sphere((tamanho[0] - 0.5) * tamanho_multiplier_the_nameless_one,
                 [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[1] + 5) * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[2] - 1.6) * tamanho_multiplier_the_nameless_one])

    # Joelho Direito
    j_d = sphere((tamanho[0] - 0.4) * tamanho_multiplier_the_nameless_one,
                 [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[1] - 1.8) * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[2] - 9.7) * tamanho_multiplier_the_nameless_one])
    # Joelho Esquerdo
    j_e = sphere((tamanho[0] - 0.4) * tamanho_multiplier_the_nameless_one,
                 [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[1] + 1.8) * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[2] - 9.7) * tamanho_multiplier_the_nameless_one])

    ###########Braços##########
    # Direito
    # Superior
    bd_s = cylinder([(tamanho[0] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[1] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[2] - 0.3) * tamanho_multiplier_the_nameless_one],
                    [(coordenadas_xyz[0] + 0) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[1] - 4) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[2] - 2.3) * tamanho_multiplier_the_nameless_one])
    bd_s.rotation_euler = (pi / 2, 2, 0)

    # Inferior
    bd_i = cylinder([(tamanho[0] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[1] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[2] - 0.3) * tamanho_multiplier_the_nameless_one],
                    [(coordenadas_xyz[0] + 0) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[1] - 6) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[2] - 2.3) * tamanho_multiplier_the_nameless_one])
    bd_i.rotation_euler = (pi / 2, 2, 0)

    # Esquerdo
    # Superior
    be_s = cylinder([(tamanho[0] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[1] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[2] - 0.3) * tamanho_multiplier_the_nameless_one],
                    [(coordenadas_xyz[0] + 0) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[1] + 4) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[2] - 2.3) * tamanho_multiplier_the_nameless_one])
    be_s.rotation_euler = (pi / 2, 2, 0)

    # Inferior
    be_i = cylinder([(tamanho[0] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[1] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[2] - 0.3) * tamanho_multiplier_the_nameless_one],
                    [(coordenadas_xyz[0] + 0) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[1] + 6) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[2] - 2.3) * tamanho_multiplier_the_nameless_one])
    be_i.rotation_euler = (pi / 2, 2, 0)

    # Mãos
    m_e = sphere((tamanho[0] - 0.5) * tamanho_multiplier_the_nameless_one,
                 [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[1] + 7) * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[2] - 1.6) * tamanho_multiplier_the_nameless_one])
    m_d = sphere((tamanho[0] - 0.5) * tamanho_multiplier_the_nameless_one,
                 [coordenadas_xyz[0] * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[1] - 7) * tamanho_multiplier_the_nameless_one,
                  (coordenadas_xyz[2] - 1.6) * tamanho_multiplier_the_nameless_one])

    ############Pernas########
    # Direita
    # Superior
    pd_s = cylinder([(tamanho[0] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[1] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[2] + 0.1) * tamanho_multiplier_the_nameless_one],
                    [(coordenadas_xyz[0] + 0.025) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[1] - 1.8) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[2] - 9.3) * tamanho_multiplier_the_nameless_one])

    # Inferior
    pd_i = cylinder([(tamanho[0] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[1] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[2] + 0.7) * tamanho_multiplier_the_nameless_one],
                    [(coordenadas_xyz[0] + 0.025) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[1] - 1.8) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[2] - 13.5) * tamanho_multiplier_the_nameless_one])

    # Esquerda
    # Superior
    pe_s = cylinder([(tamanho[0] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[1] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[2] + 0.1) * tamanho_multiplier_the_nameless_one],
                    [(coordenadas_xyz[0] + 0.025) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[1] + 1.8) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[2] - 9.3) * tamanho_multiplier_the_nameless_one])
    # Inferior
    pe_i = cylinder([(tamanho[0] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[1] - 0.6) * tamanho_multiplier_the_nameless_one,
                     (tamanho[2] + 0.7) * tamanho_multiplier_the_nameless_one],
                    [(coordenadas_xyz[0] + 0.025) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[1] + 1.8) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[2] - 13.5) * tamanho_multiplier_the_nameless_one])

    # Pes
    # Direito
    ped1 = cylinder([(tamanho[0] - 0.1) * tamanho_multiplier_the_nameless_one,
                     (tamanho[1] - 0.4) * tamanho_multiplier_the_nameless_one,
                     (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                    [(coordenadas_xyz[0] + 0.5) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[1] - 1.8) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[2] - 13.9) * tamanho_multiplier_the_nameless_one])
    ped2 = cylinder([(tamanho[0] - 0.3) * tamanho_multiplier_the_nameless_one,
                     (tamanho[1] - 0.4) * tamanho_multiplier_the_nameless_one,
                     (tamanho[2] - 0.9) * tamanho_multiplier_the_nameless_one],
                    [(coordenadas_xyz[0] + 0.3) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[1] - 1.8) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[2] - 13.5) * tamanho_multiplier_the_nameless_one])

    # Esquerdo
    pee1 = cylinder([(tamanho[0] - 0.1) * tamanho_multiplier_the_nameless_one,
                     (tamanho[1] - 0.4) * tamanho_multiplier_the_nameless_one,
                     (tamanho[2] - 0.8) * tamanho_multiplier_the_nameless_one],
                    [(coordenadas_xyz[0] + 0.5) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[1] + 1.8) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[2] - 13.9) * tamanho_multiplier_the_nameless_one])
    pee2 = cylinder([(tamanho[0] - 0.3) * tamanho_multiplier_the_nameless_one,
                     (tamanho[1] - 0.4) * tamanho_multiplier_the_nameless_one,
                     (tamanho[2] - 0.9) * tamanho_multiplier_the_nameless_one],
                    [(coordenadas_xyz[0] + 0.3) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[1] + 1.8) * tamanho_multiplier_the_nameless_one,
                     (coordenadas_xyz[2] - 13.5) * tamanho_multiplier_the_nameless_one])

    #########################################
    ##                                     ##
    ##   JUNCOES OBJETOS PRE ANIMACAO      ##
    ##          THE NAMELESS ONE             ##
    #########################################
    cabeca = []
    torso = []
    braco_esquerdo_superior = []
    braco_esquerdo_inferior = []
    braco_direito_superior = []
    braco_direito_inferior = []
    perna_direita_superior = []
    perna_direita_inferior = []
    perna_esquerda_superior = []
    perna_esquerda_inferior = []

    # CABEÇA
    # Nota: verificar se os cortes sao guardados corretamente --Helder
    cabeca.append(c1)
    cabeca.append(p1)
    cabeca.append(p2)
    cabeca.append(p3)
    cabeca.append(olh_d)
    cabeca.append(olh_e)
    cabeca.append(ore_d)
    cabeca.append(ore_e)
    cabeca.append(antena1)
    cabeca.append(antena2)
    cabeca.append(pompom1)
    cabeca.append(pompom2)
    cabeca.append(b)

    cabeca = join_objects_v2(cabeca)
    cabeca.name = "TNOcabeca"
    #########COR############
    mat_purple = bpy.data.materials.new("purple")
    mat_purple.diffuse_color = (3, 0, 0.5)
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_purple)

    # cabeca.shade_smooth()
    # BOCA

    # TORSO
    torso.append(t_s1)
    torso.append(t_s2)
    torso.append(t_s3)
    torso.append(t_s4)
    torso.append(t_s5)
    torso.append(t_s6)
    torso.append(t_s7)
    torso.append(t_s8)
    torso.append(t_s9)
    torso.append(t_c)
    torso.append(t_i1)
    torso.append(t_i2)
    torso.append(t_i3)
    torso.append(t_i4)
    torso.append(t_i5)

    torso = join_objects_v2(torso)
    torso.name = "TNOtorso"
    mat_cyan = bpy.data.materials.new("cyan")
    mat_cyan.diffuse_color = (0, 2, 2.5)
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_cyan)

    # BRAÇO ESQUERDO SUPERIOR

    braco_esquerdo_superior.append(be_s)
    braco_esquerdo_superior.append(om_e)

    braco_esquerdo_superior = join_objects_v2(braco_esquerdo_superior)
    braco_esquerdo_superior.name = "TNObraco_es"
    #########COR############
    mat_purple = bpy.data.materials.new("purple")
    mat_purple.diffuse_color = (3, 0, 0.5)
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_purple)

    # BRAÇO ESQUERDO INFERIOR
    braco_esquerdo_inferior.append(m_e)
    braco_esquerdo_inferior.append(be_i)
    braco_esquerdo_inferior.append(c_e)

    braco_esquerdo_inferior = join_objects_v2(braco_esquerdo_inferior)
    braco_esquerdo_inferior.name = "TNObraco_ei"
    #########COR############
    mat_purple = bpy.data.materials.new("purple")
    mat_purple.diffuse_color = (3, 0, 0.5)
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_purple)

    # BRAÇO DIREITO SUPERIOR

    braco_direito_superior.append(bd_s)
    braco_direito_superior.append(om_d)

    braco_direito_superior = join_objects_v2(braco_direito_superior)
    braco_direito_superior.name = "TNObraco_ds"
    #########COR############
    mat_purple = bpy.data.materials.new("purple")
    mat_purple.diffuse_color = (3, 0, 0.5)
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_purple)

    # BRAÇO DIREITO INFERIOR
    braco_direito_inferior.append(m_d)
    braco_direito_inferior.append(bd_i)
    braco_direito_inferior.append(c_d)

    braco_direito_inferior = join_objects_v2(braco_direito_inferior)
    braco_direito_inferior.name = "TNObraco_di"
    #########COR############
    mat_purple = bpy.data.materials.new("purple")
    mat_purple.diffuse_color = (3, 0, 0.5)
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_purple)

    # PERNA ESQUERDO SUPERIOR

    perna_esquerda_superior.append(pe_s)
    perna_esquerda_superior.append(a_e)

    perna_esquerda_superior = join_objects_v2(perna_esquerda_superior)
    perna_esquerda_superior.name = "TNOperna_es"
    #########COR############
    mat_purple = bpy.data.materials.new("purple")
    mat_purple.diffuse_color = (3, 0, 0.5)
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_purple)

    # PERNA ESQUERDO INFERIOR

    perna_esquerda_inferior.append(pe_i)
    perna_esquerda_inferior.append(j_e)
    perna_esquerda_inferior.append(pee1)
    perna_esquerda_inferior.append(pee2)

    perna_esquerda_inferior = join_objects_v2(perna_esquerda_inferior)
    perna_esquerda_inferior.name = "TNOperna_ei"
    #########COR############
    mat_purple = bpy.data.materials.new("purple")
    mat_purple.diffuse_color = (3, 0, 0.5)
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_purple)

    # PERNA DIREITO SUPERIOR

    perna_direita_superior.append(pd_s)
    perna_direita_superior.append(a_d)

    perna_direita_superior = join_objects_v2(perna_direita_superior)
    perna_direita_superior.name = "TNOperna_ds"
    #########COR############
    mat_purple = bpy.data.materials.new("purple")
    mat_purple.diffuse_color = (3, 0, 0.5)
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_purple)

    # PERNA DIREITO INFERIOR

    perna_direita_inferior.append(pd_i)
    perna_direita_inferior.append(j_d)
    perna_direita_inferior.append(ped1)
    perna_direita_inferior.append(ped2)

    perna_direita_inferior = join_objects_v2(perna_direita_inferior)
    perna_direita_inferior.name = "TNOperna_di"
    #########COR############
    mat_purple = bpy.data.materials.new("purple")
    mat_purple.diffuse_color = (3, 0, 0.5)
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_purple)
    bpy.ops.object.select_all(action='DESELECT')

    # The_Nameless_one=[cabeca, boca, torso,braco_esquerdo_superior,braco_esquerdo_inferior, braco_esquerdo_mao,braco_direito_superior, braco_direito_inferior,braco_direito_mao, perna_direita_superior, perna_direita_inferior, perna_esquerda_superior, perna_esquerda_inferior]
    The_Nameless_one = []
    The_Nameless_one.append(cabeca)
    The_Nameless_one.append(torso)
    The_Nameless_one.append(braco_esquerdo_superior)
    The_Nameless_one.append(braco_esquerdo_inferior)
    The_Nameless_one.append(braco_direito_superior)
    The_Nameless_one.append(braco_direito_inferior)

    The_Nameless_one.append(perna_esquerda_superior)
    The_Nameless_one.append(perna_esquerda_inferior)
    The_Nameless_one.append(perna_direita_superior)
    The_Nameless_one.append(perna_direita_inferior)

    The_Nameless_one = join_objects_v2(The_Nameless_one)
    The_Nameless_one.name = "The_Nameless_one"

def cena():
    the_nameless_one([1, 1, 1], [0, 16, 14])

# Eliminacao dos objetos posteriormente criados na cena.
delete_all_meshes_except_TIG()
delete_all_empty()

# Chama da cena
cena()

######### The Iron Giant ##############
#Build The Iron Giant
main()
#########  End of The Iron Giant ##########

#######CAVEIRA######
# Osso principal - TRONCO
bpy.ops.object.mode_set(mode='OBJECT')
bpy.context.scene.cursor_location = (0, 0, 0)
bpy.ops.object.select_all(action='DESELECT')
bpy.ops.object.armature_add(location=(0, 2.88, 1.5))
bpy.ops.transform.resize(value=(0.7, 0.7, 0.9))
obj = bpy.context.scene.objects.active
obj.name = "esqueleto"
arma = obj.data
arma.name = "Armadura"
bpy.data.objects["esqueleto"].data.bones[0].name = "tronco"

# cabeca
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.bone_primitive_add(name="cabeca")
bpy.ops.object.mode_set(mode='POSE')
bpy.data.objects["esqueleto"].data.bones["cabeca"].select = True
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.transform.resize(value=(0.6, 0.6, 0.5))
bpy.ops.transform.translate(value=(0, 2.88, 2.2))

# pernaR
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.bone_primitive_add(name="pernaR")
bpy.ops.object.mode_set(mode='POSE')
bpy.data.objects["esqueleto"].data.bones["pernaR"].select = True
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.switch_direction()
bpy.ops.transform.resize(value=(0.6, 0.6, 0.6))
bpy.ops.transform.translate(value=(0, 2.55, 0.6))

# canelaR
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.bone_primitive_add(name="canelaR")
bpy.ops.object.mode_set(mode='POSE')
bpy.data.objects["esqueleto"].data.bones["canelaR"].select = True
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.switch_direction()
bpy.ops.transform.resize(value=(0.65, 0.65, 0.65))
bpy.ops.transform.translate(value=(0, 2.55, -0.1))

# pernaL
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.bone_primitive_add(name="pernaL")
bpy.ops.object.mode_set(mode='POSE')
bpy.data.objects["esqueleto"].data.bones["pernaL"].select = True
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.switch_direction()
bpy.ops.transform.resize(value=(0.6, 0.6, 0.6))
bpy.ops.transform.translate(value=(0, 3.2, 0.6))

# canelaL
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.bone_primitive_add(name="canelaL")
bpy.ops.object.mode_set(mode='POSE')
bpy.data.objects["esqueleto"].data.bones["canelaL"].select = True
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.switch_direction()
bpy.ops.transform.resize(value=(0.65, 0.65, 0.65))
bpy.ops.transform.translate(value=(0, 3.2, -0.1))

# bracoR
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.bone_primitive_add(name="bracoR")
bpy.ops.object.mode_set(mode='POSE')
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.transform.resize(value=(0.45, 0.45, 0.45))
bpy.ops.transform.rotate(value=pi / 2, axis=(1, 0, 0))
bpy.ops.transform.translate(value=(0, 2.22, 1.73))

# antebracoR
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.bone_primitive_add(name="antebracoR")
bpy.ops.object.mode_set(mode='POSE')
bpy.data.objects["esqueleto"].data.bones["antebracoR"].select = True
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.transform.resize(value=(0.45, 0.45, 0.45))
bpy.ops.transform.rotate(value=pi / 2, axis=(1, 0, 0))
bpy.ops.transform.translate(value=(0, 1.75, 1.73))

# bracoL
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.bone_primitive_add(name="bracoL")
bpy.ops.object.mode_set(mode='POSE')
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.transform.resize(value=(0.45, 0.45, 0.45))
bpy.ops.transform.rotate(value=-pi / 2, axis=(1, 0, 0))
bpy.ops.transform.translate(value=(0, 3.55, 1.73))

# antebracoL
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.bone_primitive_add(name="antebracoL")
bpy.ops.object.mode_set(mode='POSE')
bpy.data.objects["esqueleto"].data.bones["antebracoL"].select = True
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.transform.resize(value=(0.45, 0.45, 0.45))
bpy.ops.transform.rotate(value=-pi / 2, axis=(1, 0, 0))
bpy.ops.transform.translate(value=(0, 4, 1.73))

# ajuste
bpy.ops.armature.select_all(action='SELECT')
bpy.ops.transform.translate(value=(0, 0, -0.1))
bpy.ops.armature.select_all(action='DESELECT')

# parentescos
# ao osso principal
bpy.ops.object.mode_set(mode='POSE')
bpy.data.objects["esqueleto"].data.bones["cabeca"].select = True
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.data.objects["esqueleto"].data.bones["pernaR"].select = True
bpy.data.objects["esqueleto"].data.bones["pernaL"].select = True
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.armature.parent_set(type='OFFSET')
bpy.ops.armature.select_all(action='DESELECT')

# ossos das extremidades
bpy.ops.object.mode_set(mode='EDIT')
arma.edit_bones["antebracoR"].parent = obj.data.edit_bones["bracoR"]
arma.edit_bones["antebracoL"].parent = obj.data.edit_bones["bracoL"]
arma.edit_bones["canelaR"].parent = obj.data.edit_bones["pernaR"]
arma.edit_bones["canelaL"].parent = obj.data.edit_bones["pernaL"]

bpy.ops.object.mode_set(mode='OBJECT')
bpy.ops.object.select_all(action='DESELECT')

# código para juntar depois a armadura ao boneco
bpy.ops.object.mode_set(mode='EDIT')
bpy.data.objects['The_Nameless_one'].select = True
bpy.data.objects['esqueleto'].select = True
bpy.ops.object.mode_set(mode='POSE')
bpy.ops.object.parent_set(type='ARMATURE_AUTO')
bpy.ops.object.mode_set(mode='OBJECT')

scn = bpy.context.scene
scn.frame_start = 0
scn.frame_end = 30
#################################### Animation walk cycle
# 1º keyframe
bpy.ops.object.mode_set(mode= 'POSE')
bpy.ops.pose.select_all(action='DESELECT')
bpy.context.scene.tool_settings.use_keyframe_insert_auto = True
wlk = bpy.context.object
wlk.animation_data_create()
wlk.animation_data.action = bpy.data.actions.new(name="walk")

# Braço esquerdo
bpy.data.objects["esqueleto"].data.bones["antebracoL"].select = True
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.ops.transform.rotate(value=-1.6, axis=(0, 0, 1))
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = False
bpy.ops.transform.rotate(value=-0.7, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=0.8, axis=(1, 0, 0))

# Braço direito
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["antebracoR"].select = True
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.ops.transform.rotate(value=1.3, axis=(0, 0, 1))
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = False
bpy.ops.transform.rotate(value=-0.6, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=-0.8, axis=(1, 0, 0))

# Pernas
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["pernaR"].select = True
bpy.ops.transform.rotate(value=-0.7, axis=(0, 1, 0))
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["pernaL"].select = True
bpy.ops.transform.rotate(value=0.7, axis=(0, 1, 0))
bpy.ops.pose.select_all(action='DESELECT')

# 2 keyframe walk cycle
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(15)
bpy.ops.object.mode_set(mode= 'POSE')

# Braços
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.ops.transform.rotate(value=0.3, axis=(0, 1, 0))
bpy.ops.pose.select_all(action='DESELECT')

# Pernas
bpy.data.objects["esqueleto"].data.bones["pernaR"].select = True
bpy.ops.transform.rotate(value=1.4, axis=(0, 1, 0))
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["pernaL"].select = True
bpy.ops.transform.rotate(value=-1.4, axis=(0, 1, 0))
bpy.ops.pose.select_all(action='DESELECT')

# 3 keyframe walk cycle
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(30)
bpy.ops.object.mode_set(mode= 'POSE')

# Braços
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.ops.transform.rotate(value=-0.3, axis=(0, 1, 0))
bpy.ops.pose.select_all(action='DESELECT')

# Pernas
bpy.data.objects["esqueleto"].data.bones["pernaR"].select = True
bpy.ops.transform.rotate(value=-1.4, axis=(0, 1, 0))
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["pernaL"].select = True
bpy.ops.transform.rotate(value=1.4, axis=(0, 1, 0))
bpy.ops.pose.select_all(action='DESELECT')

bpy.data.actions["walk"].use_fake_user = True
bpy.context.scene.tool_settings.use_keyframe_insert_auto = False


################################ Move front static mesh The Nameless One
#### Sensor
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.ops.object.select_all(action='DESELECT')
bpy.ops.logic.sensor_add(type = 'KEYBOARD', name = 'sensorWalk', object = 'The_Nameless_one')
bpy.data.objects['The_Nameless_one'].game.sensors['sensorWalk'].key = ("W")

#### Controller
bpy.ops.logic.controller_add(type='LOGIC_AND', name="controllerWalk", object = 'The_Nameless_one')

#### Actuator
bpy.ops.logic.actuator_add(type = 'MOTION', name = 'actuatorWalk', object = 'The_Nameless_one')
bpy.data.objects['The_Nameless_one'].game.actuators['actuatorWalk'].mode='OBJECT_CHARACTER'
bpy.data.objects['The_Nameless_one'].game.actuators['actuatorWalk'].offset_location[0] = 0.05

#### Ligar Sensors com controllers e actuators
bpy.data.objects['The_Nameless_one'].game.controllers['controllerWalk'].link(sensor=bpy.data.objects['The_Nameless_one'].game.sensors['sensorWalk'], actuator = bpy.data.objects['The_Nameless_one'].game.actuators['actuatorWalk'])

################################### Armature front Walk The Nameless One
#### Sensor
bpy.ops.logic.sensor_add(type = 'KEYBOARD', name = 'sensorAnimWalk', object = 'esqueleto')
bpy.data.objects['esqueleto'].game.sensors['sensorAnimWalk'].key = ("W")

#### Controller
bpy.ops.logic.controller_add(type='LOGIC_AND', name="controllerAnimWalk", object = 'esqueleto')

#### Actuator
bpy.ops.logic.actuator_add(type = 'ACTION', name = 'actuatorAnimWalk', object = 'esqueleto')
bpy.data.objects['esqueleto'].game.actuators['actuatorAnimWalk'].play_mode='LOOPSTOP'
bpy.data.objects['esqueleto'].game.actuators['actuatorAnimWalk'].action=bpy.data.actions['walk']
bpy.data.objects['esqueleto'].game.actuators['actuatorAnimWalk'].frame_end=30

#### Ligar Sensors com controllers e actuators
bpy.data.objects['esqueleto'].game.controllers['controllerAnimWalk'].link(sensor=bpy.data.objects['esqueleto'].game.sensors['sensorAnimWalk'], actuator = bpy.data.objects['esqueleto'].game.actuators['actuatorAnimWalk'])

################################ Move back static mesh The Nameless One
#### Sensor
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.ops.object.select_all(action='DESELECT')
bpy.ops.logic.sensor_add(type = 'KEYBOARD', name = 'sensorWalkBack', object = 'The_Nameless_one')
bpy.data.objects['The_Nameless_one'].game.sensors['sensorWalkBack'].key = ("S")

#### Controller
bpy.ops.logic.controller_add(type='LOGIC_AND', name="controllerWalkBack", object = 'The_Nameless_one')

#### Actuator
bpy.ops.logic.actuator_add(type = 'MOTION', name = 'actuatorWalkBack', object = 'The_Nameless_one')
bpy.data.objects['The_Nameless_one'].game.actuators['actuatorWalkBack'].mode='OBJECT_CHARACTER'
bpy.data.objects['The_Nameless_one'].game.actuators['actuatorWalkBack'].offset_location[0] =-0.05

#### Ligar Sensors com controllers e actuators
bpy.data.objects['The_Nameless_one'].game.controllers['controllerWalkBack'].link(sensor=bpy.data.objects['The_Nameless_one'].game.sensors['sensorWalkBack'], actuator = bpy.data.objects['The_Nameless_one'].game.actuators['actuatorWalkBack'])

################################### Armature back Walk The Nameless One
#### Sensor
bpy.ops.logic.sensor_add(type = 'KEYBOARD', name = 'sensorAnimWalkBack', object = 'esqueleto')
bpy.data.objects['esqueleto'].game.sensors['sensorAnimWalkBack'].key = ("S")

#### Controller
bpy.ops.logic.controller_add(type='LOGIC_AND', name="controllerAnimWalkBack", object = 'esqueleto')

#### Actuator
bpy.ops.logic.actuator_add(type = 'ACTION', name = 'actuatorAnimWalkBack', object = 'esqueleto')
bpy.data.objects['esqueleto'].game.actuators['actuatorAnimWalkBack'].play_mode='LOOPSTOP'
bpy.data.objects['esqueleto'].game.actuators['actuatorAnimWalkBack'].action=bpy.data.actions['walk']
bpy.data.objects['esqueleto'].game.actuators['actuatorAnimWalkBack'].frame_end=30

#### Ligar Sensors com controllers e actuators
bpy.data.objects['esqueleto'].game.controllers['controllerAnimWalkBack'].link(sensor=bpy.data.objects['esqueleto'].game.sensors['sensorAnimWalkBack'], actuator = bpy.data.objects['esqueleto'].game.actuators['actuatorAnimWalkBack'])

############################ Animation Punch cycle
scn3 = bpy.context.scene
scn3.frame_start = 0
scn3.frame_end = 10
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(0)

# 1 keyframe
bpy.ops.object.mode_set(mode= 'POSE')
bpy.ops.pose.select_all(action='SELECT')
bpy.ops.pose.rot_clear()
bpy.ops.pose.scale_clear()
bpy.ops.pose.loc_clear()

bpy.context.scene.tool_settings.use_keyframe_insert_auto = True
punch = bpy.context.object
punch.animation_data_create()
punch.animation_data.action = bpy.data.actions.new(name="Punch")

# Braço esquerdo
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["antebracoL"].select = True
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.ops.transform.rotate(value=-1.6, axis=(0, 0, 1))
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = False
bpy.ops.transform.rotate(value=-0.7, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=0.8, axis=(1, 0, 0))

# Braço direito
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["antebracoR"].select = True
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.ops.transform.rotate(value=1.3, axis=(0, 0, 1))
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = False
bpy.ops.transform.rotate(value=-0.6, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=-0.8, axis=(1, 0, 0))
bpy.ops.pose.select_all(action='DESELECT')
# 2 keyframe walk cycle
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(5)
bpy.ops.object.mode_set(mode= 'POSE')

# Braços
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.ops.transform.translate(value=(0.1, 0, 0))
bpy.ops.transform.rotate(value=-0.2, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=0.3, axis=(0, 0, 1))
bpy.ops.pose.select_all(action='DESELECT')

bpy.data.objects["esqueleto"].data.bones["antebracoR"].select = True
bpy.ops.transform.rotate(value=0.7, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=-0.3, axis=(0, 0, 1))
bpy.ops.transform.translate(value=(0, -0.03, 0))
bpy.ops.pose.select_all(action='DESELECT')

bpy.data.objects["esqueleto"].data.bones["tronco"].select = True
bpy.ops.transform.rotate(value=0.1, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=0.2, axis=(0, 0, 1))
bpy.ops.pose.select_all(action='DESELECT')
bpy.context.scene.frame_set(0)
bpy.ops.pose.rot_clear()
# 3 keyframe
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(10)
bpy.ops.object.mode_set(mode= 'POSE')

bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.ops.transform.translate(value=(-0.1, 0, 0))
bpy.ops.transform.rotate(value=0.2, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=-0.3, axis=(0, 0, 1))
bpy.ops.pose.select_all(action='DESELECT')

bpy.data.objects["esqueleto"].data.bones["antebracoR"].select = True
bpy.ops.transform.rotate(value=-0.7, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=0.3, axis=(0, 0, 1))
bpy.ops.transform.translate(value=(0, 0.03, 0))
bpy.ops.pose.select_all(action='DESELECT')

bpy.data.objects["esqueleto"].data.bones["tronco"].select = True
bpy.ops.transform.rotate(value=-0.1, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=-0.2, axis=(0, 0, 1))

bpy.context.scene.frame_set(0)
bpy.ops.pose.rot_clear()

bpy.ops.pose.select_all(action='DESELECT')

bpy.data.actions["Punch"].use_fake_user = True
bpy.context.scene.tool_settings.use_keyframe_insert_auto = False
#bpy.ops.pose.select_all(action='DESELECT')

############################## Armature Punch The Nameless One
#### Sensor
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.ops.object.select_all(action='DESELECT')
bpy.ops.logic.sensor_add(type = 'KEYBOARD', name = 'sensorPunch', object = 'esqueleto')
bpy.data.objects['esqueleto'].game.sensors['sensorPunch'].key = ('ONE')

#### Controller
bpy.ops.logic.controller_add(type='LOGIC_AND', name="controllerPunch", object = 'esqueleto')

#### Actuator
bpy.ops.logic.actuator_add(type = 'ACTION', name = 'actuatorPunch', object = 'esqueleto')
bpy.data.objects['esqueleto'].game.actuators['actuatorPunch'].play_mode='LOOPEND'
bpy.data.objects['esqueleto'].game.actuators['actuatorPunch'].action=bpy.data.actions['Punch']
bpy.data.objects['esqueleto'].game.actuators['actuatorPunch'].priority = 0
bpy.data.objects['esqueleto'].game.actuators['actuatorPunch'].frame_end=10

#### Ligar Sensors com controllers e actuators
bpy.data.objects['esqueleto'].game.controllers['controllerPunch'].link(sensor=bpy.data.objects['esqueleto'].game.sensors['sensorPunch'], actuator = bpy.data.objects['esqueleto'].game.actuators['actuatorPunch'])

############################################ Animation idle cycle
scn2 = bpy.context.scene
scn2.frame_start = 0
scn2.frame_end = 30
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(0)

# 1 keyframe
bpy.ops.object.mode_set(mode= 'POSE')
bpy.ops.pose.select_all(action='SELECT')
bpy.ops.pose.rot_clear()
bpy.ops.pose.scale_clear()
bpy.ops.pose.loc_clear()

bpy.context.scene.tool_settings.use_keyframe_insert_auto = True
idle = bpy.context.object
idle.animation_data_create()
idle.animation_data.action = bpy.data.actions.new(name="idle")

# Braço esquerdo
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["antebracoL"].select = True
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.ops.transform.rotate(value=-1.6, axis=(0, 0, 1))
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = False
bpy.ops.transform.rotate(value=-0.7, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=0.8, axis=(1, 0, 0))

# Braço direito
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["antebracoR"].select = True
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.ops.transform.rotate(value=1.3, axis=(0, 0, 1))
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = False
bpy.ops.transform.rotate(value=-0.6, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=-0.8, axis=(1, 0, 0))

# 2 Keyframe
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(15)
bpy.ops.object.mode_set(mode= 'POSE')

# Braços
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.ops.transform.rotate(value=0.3, axis=(0, 1, 0))
bpy.ops.pose.select_all(action='DESELECT')

# 3 Keyframe
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(30)
bpy.ops.object.mode_set(mode= 'POSE')

bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.ops.transform.rotate(value=-0.3, axis=(0, 1, 0))
bpy.ops.pose.select_all(action='DESELECT')

bpy.data.actions["idle"].use_fake_user = True
bpy.context.scene.tool_settings.use_keyframe_insert_auto = False

############################## Armature Idle The Nameless One
#### Sensor
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.ops.object.select_all(action='DESELECT')
bpy.ops.logic.sensor_add(type = 'KEYBOARD', name = 'sensorIdle', object = 'esqueleto')
bpy.data.objects['esqueleto'].game.sensors['sensorIdle'].invert = True
bpy.data.objects['esqueleto'].game.sensors['sensorIdle'].use_all_keys = True


#### Controller
bpy.ops.logic.controller_add(type='LOGIC_AND', name="controllerIdle", object = 'esqueleto')

#### Actuator
bpy.ops.logic.actuator_add(type = 'ACTION', name = 'actuatorIdle', object = 'esqueleto')
bpy.data.objects['esqueleto'].game.actuators['actuatorIdle'].play_mode='LOOPSTOP'
bpy.data.objects['esqueleto'].game.actuators['actuatorIdle'].action=bpy.data.actions['idle']
bpy.data.objects['esqueleto'].game.actuators['actuatorIdle'].priority = 1
bpy.data.objects['esqueleto'].game.actuators['actuatorIdle'].frame_end=30

#### Ligar Sensors com controllers e actuators
bpy.data.objects['esqueleto'].game.controllers['controllerIdle'].link(sensor=bpy.data.objects['esqueleto'].game.sensors['sensorIdle'], actuator = bpy.data.objects['esqueleto'].game.actuators['actuatorIdle'])

############################ Animation Kick cycle
scn4 = bpy.context.scene
scn4.frame_start = 0
scn4.frame_end = 20
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(0)

# 1 keyframe
bpy.ops.object.mode_set(mode= 'POSE')
bpy.ops.pose.select_all(action='SELECT')
bpy.ops.pose.rot_clear()
bpy.ops.pose.scale_clear()
bpy.ops.pose.loc_clear()

bpy.context.scene.tool_settings.use_keyframe_insert_auto = True
kick = bpy.context.object
kick.animation_data_create()
kick.animation_data.action = bpy.data.actions.new(name="kick")

# Braço esquerdo
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.ops.transform.rotate(value=-1.6, axis=(0, 0, 1))
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["antebracoL"].select = True
bpy.ops.transform.rotate(value=-0.7, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=0.8, axis=(1, 0, 0))

# Braço direito
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.ops.transform.rotate(value=1.3, axis=(0, 0, 1))
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["antebracoR"].select = True
bpy.ops.transform.rotate(value=-0.6, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=-0.8, axis=(1, 0, 0))

# 2 Keyframe
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(5)
bpy.ops.object.mode_set(mode= 'POSE')

# Braços
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.data.objects["esqueleto"].data.bones["antebracoR"].select = True
bpy.data.objects["esqueleto"].data.bones["antebracoL"].select = True
bpy.ops.pose.rot_clear()
bpy.ops.pose.select_all(action='DESELECT')

# Pernas
bpy.data.objects["esqueleto"].data.bones["pernaR"].select = True
bpy.ops.transform.rotate(value=-1.7, axis=(0, 1, 0))
bpy.ops.transform.translate(value=(0.2, 0, 0.2))

bpy.ops.pose.select_all(action='DESELECT')

bpy.data.objects["esqueleto"].data.bones["pernaL"].select = True
bpy.ops.transform.rotate(value=0.3, axis=(0, 1, 0))
bpy.ops.pose.select_all(action='DESELECT')

bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(0)
bpy.ops.object.mode_set(mode= 'POSE')
bpy.data.objects["esqueleto"].data.bones["pernaR"].select = True
bpy.data.objects["esqueleto"].data.bones["pernaL"].select = True
bpy.ops.pose.rot_clear()
bpy.ops.pose.loc_clear()

# 3 Keyframe
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(20)
bpy.ops.object.mode_set(mode= 'POSE')

# Braço esquerdo
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.ops.transform.rotate(value=-1.6, axis=(0, 0, 1))
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["antebracoL"].select = True
bpy.ops.transform.rotate(value=-0.7, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=0.8, axis=(1, 0, 0))

# Braço direito
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.ops.transform.rotate(value=1.3, axis=(0, 0, 1))
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["antebracoR"].select = True
bpy.ops.transform.rotate(value=-0.6, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=-0.8, axis=(1, 0, 0))
bpy.ops.pose.select_all(action='DESELECT')

# Pernas
bpy.data.objects["esqueleto"].data.bones["pernaR"].select = True
bpy.data.objects["esqueleto"].data.bones["pernaL"].select = True
bpy.ops.pose.rot_clear()
bpy.ops.pose.loc_clear()
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.actions["kick"].use_fake_user = True
bpy.context.scene.tool_settings.use_keyframe_insert_auto = False

############################## Armature Kick The Nameless One

#### Sensor
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.ops.object.select_all(action='DESELECT')
bpy.ops.logic.sensor_add(type = 'KEYBOARD', name = 'sensorKick', object = 'esqueleto')
bpy.data.objects['esqueleto'].game.sensors['sensorKick'].key = ('TWO')

#### Controller
bpy.ops.logic.controller_add(type='LOGIC_AND', name="controllerKick", object = 'esqueleto')

#### Actuator
bpy.ops.logic.actuator_add(type = 'ACTION', name = 'actuatorKick', object = 'esqueleto')
bpy.data.objects['esqueleto'].game.actuators['actuatorKick'].play_mode='LOOPEND'
bpy.data.objects['esqueleto'].game.actuators['actuatorKick'].action=bpy.data.actions['kick']
bpy.data.objects['esqueleto'].game.actuators['actuatorKick'].priority = 0
bpy.data.objects['esqueleto'].game.actuators['actuatorKick'].frame_end=20

#### Ligar Sensors com controllers e actuators
bpy.data.objects['esqueleto'].game.controllers['controllerKick'].link(sensor=bpy.data.objects['esqueleto'].game.sensors['sensorKick'], actuator = bpy.data.objects['esqueleto'].game.actuators['actuatorKick'])

############################ Animation Block cycle
scn5 = bpy.context.scene
scn5.frame_start = 0
scn5.frame_end = 20
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(0)

# 1 keyframe
bpy.ops.object.mode_set(mode= 'POSE')
bpy.ops.pose.select_all(action='SELECT')
bpy.ops.pose.rot_clear()
bpy.ops.pose.scale_clear()
bpy.ops.pose.loc_clear()

bpy.context.scene.tool_settings.use_keyframe_insert_auto = True
block = bpy.context.object
block.animation_data_create()
block.animation_data.action = bpy.data.actions.new(name="block")
# Braço esquerdo
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.ops.transform.rotate(value=-1.6, axis=(0, 0, 1))
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["antebracoL"].select = True
bpy.ops.transform.rotate(value=-0.7, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=0.8, axis=(1, 0, 0))

# Braço direito
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.ops.transform.rotate(value=1.3, axis=(0, 0, 1))
bpy.ops.pose.select_all(action='DESELECT')
bpy.data.objects["esqueleto"].data.bones["antebracoR"].select = True
bpy.ops.transform.rotate(value=-0.6, axis=(0, 1, 0))
bpy.ops.transform.rotate(value=-0.8, axis=(1, 0, 0))
bpy.ops.pose.select_all(action='DESELECT')

# 2 Keyframe
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(5)
bpy.ops.object.mode_set(mode= 'POSE')

# Braço dreito
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.ops.transform.rotate(value=0.5, axis=(0, 0, 1))
bpy.ops.transform.rotate(value=-0.3, axis=(0, 1, 0))

bpy.ops.pose.select_all(action='DESELECT')

# Braço esquerdo
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.ops.transform.rotate(value=-0.2, axis=(0, 0, 1))
bpy.ops.transform.rotate(value=-0.4, axis=(0, 1, 0))
bpy.ops.pose.select_all(action='DESELECT')


# 3 Keyframe
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.context.scene.frame_set(15)
bpy.ops.object.mode_set(mode= 'POSE')

# Braço dreito
bpy.data.objects["esqueleto"].data.bones["bracoR"].select = True
bpy.ops.transform.rotate(value=-0.5, axis=(0, 0, 1))
bpy.ops.transform.rotate(value=0.3, axis=(0, 1, 0))

bpy.ops.pose.select_all(action='DESELECT')

# Braço esquerdo
bpy.data.objects["esqueleto"].data.bones["bracoL"].select = True
bpy.ops.transform.rotate(value=0.2, axis=(0, 0, 1))
bpy.ops.transform.rotate(value=0.4, axis=(0, 1, 0))


bpy.ops.pose.select_all(action='DESELECT')
bpy.data.actions["block"].use_fake_user = True
bpy.context.scene.tool_settings.use_keyframe_insert_auto = False


############################## Armature Block The Nameless One

#### Sensor
bpy.ops.object.mode_set(mode= 'OBJECT')
bpy.ops.object.select_all(action='DESELECT')
bpy.ops.logic.sensor_add(type = 'KEYBOARD', name = 'sensorBlock', object = 'esqueleto')
bpy.data.objects['esqueleto'].game.sensors['sensorBlock'].key = ('THREE')

#### Controller
bpy.ops.logic.controller_add(type='LOGIC_AND', name="controllerBlock", object = 'esqueleto')

#### Actuator
bpy.ops.logic.actuator_add(type = 'ACTION', name = 'actuatorBlock', object = 'esqueleto')
bpy.data.objects['esqueleto'].game.actuators['actuatorBlock'].play_mode='LOOPEND'
bpy.data.objects['esqueleto'].game.actuators['actuatorBlock'].action=bpy.data.actions['block']
bpy.data.objects['esqueleto'].game.actuators['actuatorBlock'].priority = 0
bpy.data.objects['esqueleto'].game.actuators['actuatorBlock'].frame_end=15

#### Ligar Sensors com controllers e actuators
bpy.data.objects['esqueleto'].game.controllers['controllerBlock'].link(sensor=bpy.data.objects['esqueleto'].game.sensors['sensorBlock'], actuator = bpy.data.objects['esqueleto'].game.actuators['actuatorBlock'])

