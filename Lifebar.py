import bpy
import math
from math import pi, radians, degrees
from mathutils import Vector
import bmesh
import os
import aud

def delete_object_list(stop, object_list):
    # select objects by type
    if stop > len(object_list):
        print("ERROR: ", "\nStop value: ", stop, "\nList size: ", len(object_list), "\nStop is bigger than the list size!")
    for i in range(stop):
        object_list[i].select = True
    bpy.ops.object.delete()
    for i in range(stop):
        object_list.pop(0)

def lifebar(locX):
    life = []
    for i in range(100):
        bpy.ops.mesh.primitive_cube_add(radius=0.03, location=(locX + i / 17, 10, 7))
        bpy.context.object.scale[2] = 10
        life.append(bpy.context.object)
        mat_green = bpy.data.materials.new("green")
        mat_green.diffuse_color = (0, 2, 0)
        mesh = bpy.context.object.data
        mesh.materials.clear()
        mesh.materials.append(mat_green)
    return life


BarraTNO = lifebar(5)

BarraTIG = lifebar(-10)
delete_object_list(75, BarraTNO)# o 1 parametro e a quantidade de vida a tirar, btw, acho que tens os nomes das barras trocadas
print(BarraTNO)
print(BarraTIG)